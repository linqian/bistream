# Scalable Distributed Stream Join System (BiStream)

BiStream is designed based on the join-biclique model. Visite the [project page](http://www.comp.nus.edu.sg/~linqian/bistream) for detials. 

## Building BiStream

BiStream source code is maintained using [Maven](http://maven.apache.org/). Generate the excutable jar by running

    mvn clean package

## Running BiStream

BiStream is built on top of [Storm](https://storm.apache.org/). After deploying a Storm cluster, you can launch BiStream by submitting its jar to the cluster. Please refer to Storm documents for how to [set up a Storm cluster](https://storm.apache.org/documentation/Setting-up-a-Storm-cluster.html) and [run topologies on a Storm cluster](https://storm.apache.org/documentation/Running-topologies-on-a-production-cluster.html). 
