package soj.biclique.bolt;

import java.util.List;

import com.google.common.collect.ImmutableList;
import static com.google.common.collect.Lists.newArrayList;

import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import static soj.biclique.JoinBicliqueTopology.BROADCAST_R_STREAM_ID;
import static soj.biclique.JoinBicliqueTopology.BROADCAST_S_STREAM_ID;
import static soj.biclique.JoinBicliqueTopology.SHUFFLE_R_STREAM_ID;
import static soj.biclique.JoinBicliqueTopology.SHUFFLE_S_STREAM_ID;
import static soj.util.CastUtils.getList;

public class ReshuffleBolt extends BaseBasicBolt
{
    private static final List<String> SCHEMA = ImmutableList.of("relation",
            "timestamp", "seq", "payload");

    private static final String _streamShuffleR = SHUFFLE_R_STREAM_ID;
    private static final String _streamShuffleS = SHUFFLE_S_STREAM_ID;
    private static final String _streamBroadcastR = BROADCAST_R_STREAM_ID;
    private static final String _streamBroadcastS = BROADCAST_S_STREAM_ID;

    public void execute(Tuple tuple, BasicOutputCollector collector) {
        /* extract contents from the tuple */
        String rel = tuple.getStringByField("relation");
        if (!rel.equals("R") && !rel.equals("S")) {
            System.err.println("Unknown relation: " + rel);
            return;
        }
        Long ts = tuple.getLongByField("timestamp");
        String seq = tuple.getStringByField("seq");
        List payload = getList(tuple.getValueByField("payload"));

        Values values = new Values(rel, ts, seq, payload);

        /* shuffle to store and broadcast to join */
        if (rel.equals("R")) {
            collector.emit(_streamShuffleR, values);
            collector.emit(_streamBroadcastR, values);
        }
        else { // rel.equals("S")
            collector.emit(_streamShuffleS, values);
            collector.emit(_streamBroadcastS, values);
        }
    }

    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declareStream(_streamShuffleR, new Fields(SCHEMA));
        declarer.declareStream(_streamShuffleS, new Fields(SCHEMA));
        declarer.declareStream(_streamBroadcastR, new Fields(SCHEMA));
        declarer.declareStream(_streamBroadcastS, new Fields(SCHEMA));
    }
}