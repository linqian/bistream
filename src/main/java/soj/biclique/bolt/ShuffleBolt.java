package soj.biclique.bolt;

import java.util.List;

import com.google.common.collect.ImmutableList;
import static com.google.common.collect.Lists.newArrayList;

import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import static soj.util.CastUtils.getList;

public class ShuffleBolt extends BaseBasicBolt
{
    private static final List<String> SCHEMA = ImmutableList.of("relation",
            "timestamp", "seq", "payload");

    public void execute(Tuple tuple, BasicOutputCollector collector) {
        String rel = tuple.getStringByField("relation");
        Long ts = tuple.getLongByField("timestamp");
        String seq = tuple.getStringByField("seq");
        List payload = getList(tuple.getValueByField("payload"));

        collector.emit(new Values(rel, ts, seq, payload));
    }

    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields(SCHEMA));
    }
}