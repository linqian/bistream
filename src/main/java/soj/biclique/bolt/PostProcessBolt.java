package soj.biclique.bolt;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

import com.google.common.base.Joiner;
import static com.google.common.base.Preconditions.checkArgument;
import com.google.common.collect.ImmutableList;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSetWithExpectedSize;
import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;
import static com.google.common.hash.Hashing.murmur3_32;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import soj.util.FileWriter;
import soj.util.Stopwatch;
import static soj.util.CastUtils.getBoolean;
import static soj.util.CastUtils.getInt;
import static soj.util.CastUtils.getList;
import static soj.util.CastUtils.getLong;
import static soj.util.CastUtils.getString;

public class PostProcessBolt extends BaseRichBolt
{
    private static final Logger LOG = getLogger(PostProcessBolt.class);

    private static final List<String> SCHEMA = ImmutableList.of("count", "sum",
            "min", "max");

    private static final double PERCENTAGE_THRH_FOR_STANDBY = 0.5;
    private static final double PERCENTAGE_THRH_FOR_SWAP = 0.9;

    private OutputCollector _collector;

    private FileWriter _output;
    private boolean _simple;

    private boolean _dedup;
    private HashFunction _hashFunc;
    private HashSet<HashCode> _set;
    private HashSet<HashCode> _setStandby;

    private int _thrhStandby;
    private int _thrhSwap;

    private long _count;
    private long _countDup;

    private boolean _aggregate;
    private long _sum;
    private long _min;
    private long _max;

    private long _aggReportInMillis;
    private long _triggerEmitInMillis;
    private Stopwatch _stopwatch;

    private Joiner _joiner;

    public void prepare(Map conf, TopologyContext context,
            OutputCollector collector) {
        _collector = collector;

        if (getBoolean(conf.get("noOutput"))) {
            _output = null;
        }
        else {
            String outputDir = getString(conf.get("outputDir"));
            if (outputDir == null) {
                _output = new FileWriter(System.out);
            }
            else {
                String prefix = "m" + context.getThisTaskId();
                _output = new FileWriter(outputDir, prefix, "out");
            }
        }

        _simple = getBoolean(conf.get("simple"));

        _dedup = getBoolean(conf.get("dedup"));
        int setCapacity = getInt(conf.get("dedupSize"));
        if (_dedup) {
            _hashFunc = murmur3_32();
            _set = newHashSetWithExpectedSize(setCapacity);
            _setStandby = newHashSetWithExpectedSize(setCapacity);
        }

        _thrhStandby = (int) (setCapacity * PERCENTAGE_THRH_FOR_STANDBY);
        _thrhSwap = (int) (setCapacity * PERCENTAGE_THRH_FOR_SWAP);

        _count = 0;
        _countDup = 0;

        _aggregate = getBoolean(conf.get("aggregate"));
        if (_aggregate) {
            _sum = 0;
            _min = Long.MAX_VALUE;
            _max = Long.MIN_VALUE;
        }

        _aggReportInMillis = getLong(conf.get("aggReportInSeconds")) * 1000;
        _triggerEmitInMillis = (long) (_aggReportInMillis * 0.8 * Math.random());
        _stopwatch = Stopwatch.createStarted();

        LOG.info("dedup:" + _dedup + ", capacity:" + setCapacity
                + ", threshold_standby:" + _thrhStandby + ", threshold_swap:"
                + _thrhSwap + ", aggregate:" + _aggregate + ", output:"
                + (_output != null) + ", simple:" + _simple);

        _joiner = Joiner.on(", ").skipNulls();
    }

    public void execute(Tuple tuple) {
        String seqResult = _joiner.join(tuple.getStringByField("R:seq"),
                tuple.getStringByField("S:seq"));

        if (_dedup) {
            HashCode hashCode = hash(seqResult);
            if (isDup(hashCode)) {
                LOG.debug("Dup detected (eliminated): " + seqResult);
                ++_countDup;
                _collector.ack(tuple);
                return;
            }
            else { // not a duplicate
                register(hashCode);
            }
        }

        ++_count;

        String joinVal = getString(tuple.getValueByField("join-key"));

        if (_output != null) {
            if (_simple) {
                // outputSimple(joinVal, seqResult);
                Long tsR = tuple.getLongByField("R:timestamp");
                Long tsS = tuple.getLongByField("S:timestamp");
                outputLatency(tsR, tsS);
            }
            else {
                Long tsR = tuple.getLongByField("R:timestamp");
                Long tsS = tuple.getLongByField("S:timestamp");
                List payloadR = getList(tuple.getValueByField("R:payload"));
                List payloadS = getList(tuple.getValueByField("S:payload"));

                output(joinVal, seqResult, tsR, payloadR, tsS, payloadS);
            }
        }

        if (_aggregate)
            aggregate(getLong(joinVal));

        if (isTimeToReportAggregate()) {
            reportAggregate();
            LOG.debug("Aggregation reported @ " + _stopwatch.elapsed(SECONDS)
                    + " sec");
        }

        _collector.ack(tuple);
    }

    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields(SCHEMA));
    }

    @Override
    public void cleanup() {
        StringBuilder sb = new StringBuilder();

        sb.append("results:" + _count);

        if (_aggregate) {
            sb.append(", sum:" + _sum);
            sb.append(", min:" + _min);
            sb.append(", max:" + _max);
        }

        if (_dedup) {
            sb.append(", eliminated_duplicates:" + _countDup);
            sb.append(", primary_set_size:" + _set.size());
            sb.append(", standby_set_size:" + _setStandby.size());
        }

        long uptimeInSeconds = _stopwatch.stop().elapsed(SECONDS);
        sb.append(", uptime(sec):" + uptimeInSeconds);

        output(sb.toString());

        if (_output != null)
            _output.endOfFile();
    }

    private HashCode hash(String str) {
        return _hashFunc.hashBytes(str.getBytes());
    }

    private boolean isDup(HashCode item) {
        return _set.contains(item);
    }

    private void register(HashCode item) {
        _set.add(item);

        if (_set.size() > _thrhStandby) {
            _setStandby.add(item);
        }

        if (_set.size() > _thrhSwap) {
            HashSet<HashCode> temp = _set;
            _set = _setStandby;
            _setStandby = temp;

            _setStandby.clear();
        }
    }

    private void output(String msg) {
        if (_output != null)
            _output.write(msg);
    }

    private void output(String joinVal, String seq, Long tsR, List payloadR,
            Long tsS, List payloadS) {
        long tsDiff = tsR - tsS;
        String content = _joiner.join(payloadR, payloadS);

        output(seq + " | " + tsDiff + " | " + joinVal + " | " + content);
    }

    private void outputSimple(String joinVal, String seq) {
        output(seq + " | " + joinVal);
    }

    private void outputLatency(Long tsR, Long tsS) {
        long tsMoreRecent = Math.max(tsR, tsS);
        long tsJoinResult = System.currentTimeMillis();
        output("Latency: " + (tsJoinResult - tsMoreRecent) + " ms");
    }

    private void aggregate(long value) {
        _sum += value;
        _min = Math.min(_min, value);
        _max = Math.max(_max, value);
    }

    private boolean isTimeToReportAggregate() {
        long currTime = _stopwatch.elapsed(MILLISECONDS);

        if (currTime >= _triggerEmitInMillis) {
            _triggerEmitInMillis = currTime + _aggReportInMillis;
            return true;
        }
        else {
            return false;
        }
    }

    private void reportAggregate() {
        Long count = Long.valueOf(_count);
        Long sum = null;
        Long min = null;
        Long max = null;
        if (_aggregate) {
            sum = Long.valueOf(_sum);
            min = Long.valueOf(_min);
            max = Long.valueOf(_max);
        }

        _collector.emit(new Values(count, sum, min, max));
    }
}