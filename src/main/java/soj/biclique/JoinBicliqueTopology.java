package soj.biclique;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

import backtype.storm.Config;
import backtype.storm.generated.StormTopology;
import backtype.storm.topology.BoltDeclarer;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.tuple.Fields;
import soj.biclique.bolt.AggregateBolt;
import soj.biclique.bolt.JoinBolt;
import soj.biclique.bolt.PostProcessBolt;
import soj.biclique.bolt.ReshuffleBolt;
import soj.biclique.bolt.ShuffleBolt;
import soj.biclique.spout.RandomDataSpout;
import soj.util.FileWriter;
import static soj.util.LogHelpers.logTopology;
import static soj.util.StormRunner.runInCluster;
import static soj.util.StormRunner.runLocally;

public class JoinBicliqueTopology
{
    private static final Logger LOG = getLogger(JoinBicliqueTopology.class);

    private static final String SPOUT_ID = "random-data";
    private static final String SPOUT_R_ID = "random-data-r";
    private static final String SPOUT_S_ID = "random-data-s";
    private static final String SHUFFLE_BOLT_ID = "shuffler";
    private static final String RESHUFFLE_BOLT_ID = "reshuffler";
    private static final String JOINER_R_BOLT_ID = "joiner-r";
    private static final String JOINER_S_BOLT_ID = "joiner-s";
    private static final String POST_PROCESS_BOLT_ID = "gatherer";
    private static final String AGGREGATE_BOLT_ID = "aggregator";

    public static final String SHUFFLE_R_STREAM_ID = "shuffle-r";
    public static final String SHUFFLE_S_STREAM_ID = "shuffle-s";
    public static final String BROADCAST_R_STREAM_ID = "broadcast-r";
    public static final String BROADCAST_S_STREAM_ID = "broadcast-s";

    private final TopologyArgs _args = new TopologyArgs("JoinBicliqueTopology");

    public int run(String[] args) throws Exception {
        if (!_args.processArgs(args))
            return -1;
        if (_args.help)
            return 0;
        else
            // _args.logArgs();
            writeSettingsToFile();

        /* build topology */
        StormTopology topology = createTopology();
        if (topology == null)
            return -2;
        logTopology(LOG, topology);

        /* configure topology */
        Config conf = configureTopology();
        if (conf == null)
            return -3;
        LOG.info("configuration: " + conf.toString());

        /* run topology */
        if (_args.remoteMode) {
            LOG.info("execution mode: remote");
            runInCluster(_args.topologyName, topology, conf);
        }
        else {
            LOG.info("execution mode: local");
            writeSettingsToFile();
            runLocally(_args.topologyName, topology, conf, _args.localRuntime);
        }

        return 0;
    }

    private StormTopology createTopology() {
        RandomDataSpout generater = null;
        if (_args.numGenerators > 0) {
            generater = new RandomDataSpout();
        }
        RandomDataSpout generaterR = null;
        if (_args.numGeneratorsR > 0) {
            generaterR = new RandomDataSpout("R");
        }
        RandomDataSpout generaterS = null;
        if (_args.numGeneratorsS > 0) {
            generaterS = new RandomDataSpout("S");
        }

        ShuffleBolt shuffler = new ShuffleBolt();
        ReshuffleBolt reshuffler = new ReshuffleBolt();
        JoinBolt joinerR = new JoinBolt("R");
        JoinBolt joinerS = new JoinBolt("S");
        PostProcessBolt gatherer = new PostProcessBolt();
        AggregateBolt aggregator = new AggregateBolt();

        TopologyBuilder builder = new TopologyBuilder();

        /* connect spouts and bolts */
        BoltDeclarer shufflerDeclarer = builder.setBolt(SHUFFLE_BOLT_ID,
                shuffler, _args.numShufflers);

        if (generater != null) {
            builder.setSpout(SPOUT_ID, generater, _args.numGenerators);
            shufflerDeclarer.shuffleGrouping(SPOUT_ID);
        }
        if (generaterR != null) {
            builder.setSpout(SPOUT_R_ID, generaterR, _args.numGeneratorsR);
            shufflerDeclarer.shuffleGrouping(SPOUT_R_ID);
        }
        if (generaterS != null) {
            builder.setSpout(SPOUT_S_ID, generaterS, _args.numGeneratorsS);
            shufflerDeclarer.shuffleGrouping(SPOUT_S_ID);
        }

        builder.setBolt(RESHUFFLE_BOLT_ID, reshuffler, _args.numReshufflers)
                .shuffleGrouping(SHUFFLE_BOLT_ID);

        builder.setBolt(JOINER_R_BOLT_ID, joinerR, _args.numPartitionsR)
                .shuffleGrouping(RESHUFFLE_BOLT_ID, SHUFFLE_R_STREAM_ID)
                .allGrouping(RESHUFFLE_BOLT_ID, BROADCAST_S_STREAM_ID);

        builder.setBolt(JOINER_S_BOLT_ID, joinerS, _args.numPartitionsS)
                .shuffleGrouping(RESHUFFLE_BOLT_ID, SHUFFLE_S_STREAM_ID)
                .allGrouping(RESHUFFLE_BOLT_ID, BROADCAST_R_STREAM_ID);

        builder.setBolt(POST_PROCESS_BOLT_ID, gatherer, _args.numGatherers)
                .fieldsGrouping(JOINER_R_BOLT_ID, new Fields("join-key"))
                .fieldsGrouping(JOINER_S_BOLT_ID, new Fields("join-key"));

        builder.setBolt(AGGREGATE_BOLT_ID, aggregator, 1).globalGrouping(
                POST_PROCESS_BOLT_ID);

        return builder.createTopology();
    }

    private Config configureTopology() {
        Config conf = new Config();

        conf.setDebug(_args.debug);
        conf.setNumWorkers(_args.numWorkers);
        conf.setNumAckers(_args.numShufflers);

        conf.put("joinFieldIdxR", _args.joinFieldIdxR);
        conf.put("joinFieldIdxS", _args.joinFieldIdxS);
        conf.put("operator", _args.operator);

        if (_args.numGenerators > 0) {
            conf.put("tuplesPerSecond", _args.tuplesPerSecond);
        }
        if (_args.numGeneratorsR > 0) {
            conf.put("tuplesPerSecondR", _args.tuplesPerSecondR);
        }
        if (_args.numGeneratorsS > 0) {
            conf.put("tuplesPerSecondS", _args.tuplesPerSecondS);
        }
        conf.put("fluctuation", _args.fluctuation);

        conf.put("subindexSize", _args.subindexSize);

        conf.put("window", _args.window);
        conf.put("winR", _args.winR);
        conf.put("winS", _args.winS);

        conf.put("dedup", !_args.noDedup);
        conf.put("dedupSize", _args.dedupSize);

        conf.put("aggregate", _args.aggregate);
        conf.put("aggReportInSeconds", _args.aggReportInSeconds);

        conf.put("noOutput", _args.noOutput);
        conf.put("outputDir", _args.outputDir);
        conf.put("simple", _args.simple);

        conf.put("intLower", _args.intLower);
        conf.put("intUpper", _args.intUpper);
        conf.put("doubleLower", _args.doubleLower);
        conf.put("doubleUpper", _args.doubleUpper);
        conf.put("charsLength", _args.charsLength);

        return conf;
    }

    private void writeSettingsToFile() {
        FileWriter output = new FileWriter(_args.outputDir, "top", "txt")
                .setPrintStream(System.out);
        _args.logArgs(output);
        output.endOfFile();
    }

    public static void main(String[] args) throws Exception {
        int rc = (new JoinBicliqueTopology()).run(args);
        LOG.info("return code: " + rc);
    }
}